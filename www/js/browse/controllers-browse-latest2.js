angular.module('starter.controllers-browse-latest2', [])

.controller('BrowseLatestCtrl2', function(
  $scope, $state, $ionicPopover, $timeout, 
  $ionicSlideBoxDelegate, Auth, Categories2, 
  Products2, Wallet2, Cart, Utils) {

$scope.sliderImages = [
    {
      'src' : 'https://s3.ap-northeast-2.amazonaws.com/mobile-apps-directory/business-directory/_release/assets/1.jpg'
    },
    {
      'src' : 'https://s3.ap-northeast-2.amazonaws.com/mobile-apps-directory/business-directory/_release/assets/2.jpg'
    },
    {
      'src' : 'https://s3.ap-northeast-2.amazonaws.com/mobile-apps-directory/business-directory/_release/assets/3.jpg'
    },
    {
      'src' : 'https://s3.ap-northeast-2.amazonaws.com/mobile-apps-directory/business-directory/_release/assets/4.jpg'
    },
    {
      'src' : 'https://s3.ap-northeast-2.amazonaws.com/mobile-apps-directory/business-directory/_release/assets/5.jpg'
    }
  ];

  // communicates with the DOM
  $scope.status = {
    sortProperty: 'timestamp_creation',
    sortMethod: 'desc',
    loading: {}, // iterates over categories
    initLoading: true,
  };

  $scope.Distance = {};

  $scope.$on('$ionicView.enter', function(e) {
    // global variables
    $scope.AuthData = Auth.AuthData;
    $scope.CartList = Cart.CachedList;

    loadNew();
    loadWallet();
  });

  $scope.slideHasChanged = function($index){
      if ($index + 1 == $ionicSlideBoxDelegate.slidesCount()){  // if last index
        $timeout(function(){
          $ionicSlideBoxDelegate.slide(0);
        }, 3000)
      } 
    }

  // ---------------------------------------------------------------------------
  // Product Loading

  // init
  $scope.ProductsThumbnails = {};
  $scope.ProductsMeta = {};

  // fn load news
  function loadNew() {
    // load categories (only once)
    if (Categories2.hasOwnProperty('all')) {
        $scope.Categories = Categories2.all;
        $scope.$broadcast('scroll.refreshComplete');
    } else {
        Categories2.get().then(function(success){
            $scope.Categories = Categories2.all;
            // --> iteratively load latest productmeta from DOM
            $scope.status['initLoading'] = false;
            $scope.$broadcast('scroll.refreshComplete');
        },
        function(error){
            console.log(error);
        })
    };
  };

  // get the latest
  $scope.loadProducts = function(categoryId) {
    $scope.status['loading'][categoryId] = true;
    Products2.filter('categoryId', categoryId, $scope.status.sortProperty, LIMITVALUE_LATEST).then(
        function(ProductsMeta){
            if(ProductsMeta == null) {$scope.status['loading'][categoryId] = null;}
            else {
                $scope.status['loading'][categoryId] = false;
                $scope.ProductsMeta[categoryId] = Utils.sortArray(Utils.arrayValuesAndKeysProducts(ProductsMeta), $scope.status.sortMethod, $scope.status.sortProperty);

                // @dependencies
                loadThumbnails(ProductsMeta);
                angular.forEach($scope.ProductsMeta[categoryId], function(value, type){
                  loadDistance(value);
                });
                
            }
        },
        function(error){
            if(error == null) {$scope.status['loading'][categoryId] = null;} else {$scope.status['loading'][categoryId] = false;}
            console.log(error);
        }
    )
  };

  // fn reload
  function reloadProducts() {
    angular.forEach($scope.Categories, function(value, categoryId){
      //console.log('reloading', categoryId)
      $scope.loadProducts(categoryId);
    })
  };

  // fn load thumbnail
  function loadThumbnails(ProductsMeta) {
    $scope.ProductsThumbnails = Products2.ProductsThumbnails;
    Products2.loadThumbnails(ProductsMeta);
  };
  $scope.getThumbnail = function(productId) {
    return $scope.ProductsThumbnails[productId];
  };

  // ---------------------------------------------------------------------------
  // Formattings
  $scope.getCurrentPrice = function(currentPrice, discountPerc) {
    return Utils.getCurrentPrice(currentPrice, discountPerc);
  };
  $scope.getOldPrice = function(currentPrice, discountPerc) {
    return Utils.getOldPrice(currentPrice, discountPerc);
  };
  $scope.formatTimestamp = function(timestamp) {
    return Utils.formatTimestamp(timestamp);
  };

  // ---------------------------------------------------------------------------
  // Sorting
  $ionicPopover.fromTemplateUrl('popovers/popover-sorting2.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });
  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  $scope.selectSortProperty = function(selectedProperty, selectedSortMethod) {
    $scope.status['sortProperty']   = selectedProperty;
    $scope.status['sortMethod']     = selectedSortMethod;

    reloadProducts(); // x reload
    $scope.closePopover();
  };

  // ---------------------------------------------------------------------------
  // Wallet (my saved items)
  $scope.WalletList = Wallet2.CachedList;
  function loadWallet() {
    Wallet2.load($scope.AuthData).then(
      function(success){
        $scope.WalletList = Wallet2.CachedList;
      });
  };

  $scope.saveItem = function(productId) {
    Wallet2.buttonPressed($scope.AuthData, productId).then(
      function(success){
        $scope.WalletList = Wallet2.CachedList;
      })
  };

  $scope.walletPressedCSS = function(productId) {
    if($scope.WalletList.hasOwnProperty(productId)){
      if($scope.WalletList[productId]) {
        return 'button-pressed'
      }
    }
  };

  // ---------------------------------------------------------------------------
  // Cart
  $scope.addToCart = function(productId, ProductMeta) {
    //console.log(productId, ProductMeta)
    Cart.buttonPressed(productId, ProductMeta, $scope.getThumbnail(productId))
  };

  $scope.cartPressedCSS = function(productId) {
    if($scope.CartList.hasOwnProperty(productId)){
      if($scope.CartList[productId]) {
        return 'button-pressed'
      }
    }
  };

  // ---------------------------------------------------------------------------
  // Other

  $scope.doRefresh = function() {
    console.log('doRefresh');
    reloadProducts();
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.goTo = function(nextState) {
    $state.go(nextState)
  };

  $scope.goToCategory = function(categoryId) {
    $state.go('app.browse-category2', {categoryId: categoryId})
  };

  $scope.goToProduct = function(productId) {
    $state.go('app.product2', {productId: productId})
  };

  
  function loadDistance(ProductMeta){
    var lat = undefined;
    var lon = undefined;    

    angular.forEach(ProductMeta.value.attributes, function(value, type){
        if(type != null && type === '위치'){
          angular.forEach(ProductMeta.value.attributes[type], function(value, type){
            if(type != null){
              if(lon == undefined){
                lon = value;
              }
              lat = value; // last index
            }
          })
          if(lat && lon){
            Utils.getDistanceToOrigin(lat+','+lon).then(function(distance) {
              console.log(ProductMeta.key);
              $scope.Distance[ProductMeta.key] = distance;
              //productMeta.distance = distance;
              //$scope.status['distance'] = true;
            });
          }
        }
    })    
  }
})
