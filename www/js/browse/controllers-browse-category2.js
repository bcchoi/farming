angular.module('starter.controllers-browse-category2', [])

.controller('BrowseCategoryCtrl2', function(
  $scope, $state, $stateParams, 
  $ionicPopover, 
  Auth, Categories2, Products2, Wallet2, Cart, Utils) {
  
  // communicates with the DOM
  $scope.status = {
    sortProperty: 'timestamp_creation',
    sortMethod: 'desc',
    loading: {}, // iterates over categories
    initLoading: true,
  };

  $scope.Distance = {};

  $scope.$on('$ionicView.enter', function(e) {
    // global variables
    $scope.Categories = Categories2.all;
    $scope.AuthData = Auth.AuthData;
    $scope.CartList = Cart.CachedList;
    
    $scope.categoryId = $stateParams.categoryId;
    if($scope.categoryId) {
        loadWallet();
        $scope.loadProducts($scope.categoryId);
    } else {
        $state.go('app.browse2')
    }
  });
  
  // ---------------------------------------------------------------------------
  // Product Loading
  
  // init
  $scope.ProductsThumbnails = {};
  $scope.ProductsMeta = {};
  
  // get the latest
  $scope.loadProducts = function(categoryId) {
    console.log("loadProducts");
    $scope.status['loading'][categoryId] = true;
    Products2.filter('categoryId', categoryId, $scope.status.sortProperty, LIMITVALUE).then(
        function(ProductsMeta){
            if(ProductsMeta == null) {$scope.status['loading'][categoryId] = null;}
            else {
                $scope.status['loading'][categoryId] = false;
                $scope.ProductsMeta[categoryId] = Utils.sortArray(Utils.arrayValuesAndKeysProducts(ProductsMeta), $scope.status.sortMethod, $scope.status.sortProperty);
                
                // @dependencies
                loadThumbnails(ProductsMeta);
                angular.forEach($scope.ProductsMeta[categoryId], function(value, type){
                  loadDistance(value);
                });
            }
        },
        function(error){
            if(error == null) {$scope.status['loading'][categoryId] = null;} else {$scope.status['loading'][categoryId] = false;}
            console.log(error);
        }
    )
  };
  
  // fn load thumbnail
  function loadThumbnails(ProductsMeta) {
    $scope.ProductsThumbnails = Products2.ProductsThumbnails;
    Products2.loadThumbnails(ProductsMeta);
  };
  $scope.getThumbnail = function(productId) {
    return $scope.ProductsThumbnails[productId];
  };
  
  // ---------------------------------------------------------------------------
  // Formattings
  $scope.getCurrentPrice = function(currentPrice, discountPerc) {
    return Utils.getCurrentPrice(currentPrice, discountPerc);
  };
  $scope.getOldPrice = function(currentPrice, discountPerc) {
    return Utils.getOldPrice(currentPrice, discountPerc);
  };
  $scope.formatTimestamp = function(timestamp) {
    return Utils.formatTimestamp(timestamp);
  };
  
  // ---------------------------------------------------------------------------
  // Sorting
  $ionicPopover.fromTemplateUrl('popovers/popover-sorting2.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });
  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  $scope.selectSortProperty = function(selectedProperty, selectedSortMethod) {
    $scope.status['sortProperty']   = selectedProperty;
    $scope.status['sortMethod']     = selectedSortMethod;
    
    $scope.loadProducts($scope.categoryId); // x reload
    $scope.closePopover();
  };
  
  // ---------------------------------------------------------------------------
  // Wallet (my saved items)
  $scope.WalletList = Wallet2.CachedList;
  function loadWallet() {
    Wallet2.load($scope.AuthData).then(
      function(success){
        $scope.WalletList = Wallet2.CachedList;
      });
  };
  
  $scope.saveItem = function(productId) {
    Wallet2.buttonPressed($scope.AuthData, productId).then(
      function(success){
        $scope.WalletList = Wallet2.CachedList;
      })
  };
  
  $scope.walletPressedCSS = function(productId) {
    if($scope.WalletList.hasOwnProperty(productId)){
      if($scope.WalletList[productId]) {
        return 'button-pressed'
      }
    }
  };
  
  // ---------------------------------------------------------------------------
  // Cart
  $scope.addToCart = function(productId, ProductMeta) {
    //console.log(productId, ProductMeta)
    Cart.buttonPressed(productId, ProductMeta, $scope.getThumbnail(productId))
  };
  
  $scope.cartPressedCSS = function(productId) {
    if($scope.CartList.hasOwnProperty(productId)){
      if($scope.CartList[productId]) {
        return 'button-pressed'
      }
    }
  };
  
  // ---------------------------------------------------------------------------
  // Other
  $scope.doRefresh = function() {
    $scope.loadProducts($scope.categoryId);
    $scope.$broadcast('scroll.refreshComplete');
  };
  
  $scope.goTo = function(nextState) {
    $state.go(nextState)
  };
  
  $scope.goToCategory = function(categoryId) {
    $state.go('app.browse-category2', {categoryId: categoryId})
  };
  
  $scope.goToProduct = function(productId) {
    $state.go('app.product2', {productId: productId})
  };
  
  function loadDistance(ProductMeta){
    var lat = undefined;
    var lon = undefined;    

    angular.forEach(ProductMeta.value.attributes, function(value, type){
        if(type != null && type === '위치'){
          angular.forEach(ProductMeta.value.attributes[type], function(value, type){
            if(type != null){
              if(lon == undefined){
                lon = value;
              }
              lat = value; // last index
            }
          })
          if(lat && lon){
            Utils.getDistanceToOrigin(lat+','+lon).then(function(distance) {
              console.log(ProductMeta.key);
              $scope.Distance[ProductMeta.key] = distance;
              //productMeta.distance = distance;
              //$scope.status['distance'] = true;
            });
          }
        }
    })    
  }
})